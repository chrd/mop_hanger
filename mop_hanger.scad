/* Openscad mop hanger */
h                                  = 2; /* mm */
lower_ring_h               = 4; /* mm */
lower_ring_d               = 14; /* mm*/
upper_ring_d              = 17; /* mm*/
hanger_upper_min_h = 11; /* mm */

module lower_ring() 
{
    difference() {
    cylinder(h, upper_ring_d, upper_ring_d);
    translate([-lower_ring_d/2, -lower_ring_d/2-4, -2])
        cube([lower_ring_d, 2*lower_ring_d, 4]);
    }
}

module arch()
{
    difference() {
    cylinder(h, lower_ring_d,lower_ring_d);
        cylinder(h, 2/3 * lower_ring_d, 2/3 * lower_ring_d); 
   translate([-upper_ring_d/2, -upper_ring_d/2+6, -2])
        cube([upper_ring_d, 2*upper_ring_d, 4]);
    }
}

module upper_ring() 
{
    /* arch */
    translate([0,0,-0.5])
    arch();
    /* columns */
    translate([upper_ring_d/2, -upper_ring_d/2+4, -0.5])
    cube([upper_ring_d/3, upper_ring_d, h]);
    translate([-upper_ring_d/2-upper_ring_d/3, -upper_ring_d/2+4, -0.5])
    cube([upper_ring_d/3, upper_ring_d, h]);
    
    /* additional columns for more strength  */
    translate([upper_ring_d/2+2, -upper_ring_d/2+4, 3])
    rotate([0,90,0])
    cube([upper_ring_d/3, upper_ring_d, h]);
    
    translate([-upper_ring_d/2-upper_ring_d/3+2, -upper_ring_d/2+4, 3])
    rotate([0,90,0])
    cube([upper_ring_d/3, upper_ring_d, h]);
}

translate([0,0,-0.5])
lower_ring();
translate([0, 0, hanger_upper_min_h+1])
rotate([-90, 0, 0])
upper_ring();